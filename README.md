# Discovery Trivia API Wrapper

*Javascript Wrapper for handling calls to the internal Discovery trivia game api*

Check out the docs to the Trivia Game API at [this link]("https://bitbucket.org/sni-digital/trivia-game-lambda-api/src/master/").

## How do I use this thing?

```JavaScript
  const Trivia = require("discovery-trivia-api")
  
  const t = new Trivia({
    id: "some-user-id",
    domain: "your-domain",
    substitutionMap: {
      first_name: "Discovery",
      last_name: "Digital"
      // ...
    }
  })

  t.launch(function(res) {
    console.log(res.statusCode)
    // => 200
    console.log(res.body)
    // => { 
    //   welcome: "welcome message",
    //   tutorial: "tutorial message", 
    //   sfx: "sfx",
    //   background: "background",
    //   ...
    // }
  })
```
