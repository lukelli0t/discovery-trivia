const https = require("https");

const HOST = "7hoqcsacjc.execute-api.us-east-1.amazonaws.com";
const METHOD = 'POST';
const ROUTES = ['launch', 'question', 'solve', 'tally'];

function Trivia(opts = {}) {
  if (!opts["id"] || !opts["domain"]) throw "id and domain are required params"
  this.id = opts["id"];
  this.domain = opts["domain"];
  this.map = opts["substitutionMap"];
  this.choice = opts["choice"] || null
};

Trivia.prototype.postOptions = function (path) {
  return {
    host: HOST,
    method: METHOD,
    path: "/Prod" + path
  }
};

Trivia.prototype.postBody = function () {
  let params = { id: this.id, domain: this.domain, substitution_map: this.map };
  if (!!this.choice) params.choice = this.choice;
  return JSON.stringify(params)
};

Trivia.prototype.onError = function (err) { throw err };

Trivia.prototype.handler = function (callback) {
  return function returnedCallback(res) {
    let body = "";
    let statusCode = res.statusCode;
    res.on('data', function (chunk) { body += chunk });
    res.on('end', function () {
      callback({
        statusCode: statusCode,
        body: JSON.parse(body)
      })
    });
  }
};

for (let i = 0; i < ROUTES.length; i++) {
  const route = ROUTES[i]
  Trivia.prototype[route] = function (callback) {
    const options = this.postOptions('/' + route)
    const req = https.request(options, this.handler(callback));
    req.on('error', this.onError)
    req.write(this.postBody())
    req.end()
    return req
  };
}

module.exports = Trivia;
